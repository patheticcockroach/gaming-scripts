; For GooCubelets: OCD http://store.steampowered.com/app/460660/
;
; Instructions for use (tested in 1280x720 resolution for mouse pointer position):
; - load the script (it starts paused)
; - in the main menu, place the mouse cursor right on the "E" of "NEW GAME"
; - without moving the cursor, click once and wait for level 1 to load
; - unpause the script (F8)
; Press F8 to pause/unpause
; 
; Unlocking all the "Finish level 1 more time" achievements should take about 90 minutes,
; then you'll have 10 achievements left to do manually. You can do all but one just by pressing L,
; which will skip the current level but still counts towards the achievements

Pause
Loop{
Send, w
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, a
Sleep 400
Send, a
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, d
Sleep 400
Send, w
Sleep 400
Send, w
Sleep 400
Send, {Escape}
Sleep 500
Click
Sleep 1000
Click
Sleep 8000
}
F8::Pause