The generic Autohotkey script in this folder can be trivially used for many games.
Here are some game-specific instructions in case you need them


GooCubelets 2 (Steam app ID 416270)
http://store.steampowered.com/app/416270/
- start a new game
- modify the autopresskey_F.ahk script:
	* press "a" instead of "f" (or whatever control moves left for you)
	* sleep for a much shorter duration (eg 50 ms instead of 1000)
- run the modified script
Unlocking all achievements should take about 20 minutes


GooCubelets: The Algoorithm (Steam app ID 431270)
http://store.steampowered.com/app/431270/
- start a new game
- modify the autopresskey_F.ahk script:
	* press "a" instead of "f" (or whatever control moves left for you)
	* sleep for a much shorter duration (eg 50 ms instead of 1000)
- run the modified script
It is worth noting that there is a bug that will prevent you from falling during the
first +/-6 seconds (during which the level timer is in the negative)
Unlocking all achievements should take about an hour

SpaceShot (Steam app ID 675940)
http://store.steampowered.com/app/675940/
- start a game (I used endless mode, I don't think it matters)
- wait until you lose
- run the autoclick.ahk script
- place your mouse cursor over the restart button
- press F8 to unpause/pause the script
Unlocking all achievements should take about 1h40

Torch Cave (Steam app ID 501690)
http://store.steampowered.com/app/501690/
- start a game
- run the autopresskey_space.ahk script
- press F8 to unpause/pause the script


Torch Cave 2 (Steam app ID 533710)
http://store.steampowered.com/app/533710/
- start a game
- run the autopresskey_space.ahk script
- press F8 to unpause/pause the script


Trashville (Steam app ID 589390)
http://store.steampowered.com/app/589390/
- start a game
- make sure achievements start popping (you may need to move around a little bit)
- wait until you die (shouldn't take much more than 30 seconds)
- place the mouse cursor on "Restart"
- run the autoclick.ahk script
Unlocking all achievements (or almost all, it seems 2 are bugged) should take about 2 hours
