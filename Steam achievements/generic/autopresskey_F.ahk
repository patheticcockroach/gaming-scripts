; Initially created for Sleengster http://store.steampowered.com/app/559500/
; But it's actually a trivial script that hits the F key forever once per second
; and thus can be used for any game that requires repetitive single key presses
; (editing the key or delay is trivial)
;
; Press F8 to pause/unpause

Pause
Loop{
Send, f
Sleep 1000
}
F8::Pause
